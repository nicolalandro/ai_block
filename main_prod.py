from flask_server.configurations import ProductionConfig
from flask_server.server import app, socketio

if __name__ == "__main__":
    app.config.from_object(ProductionConfig)
    socketio.run(app, '0.0.0.0', 5000)

