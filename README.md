[![pipeline status](https://gitlab.com/nicolalandro/ai_block/badges/master/pipeline.svg)](https://gitlab.com/nicolalandro/ai_block/commits/master)
[![codecov](https://codecov.io/gl/nicolalandro/ai_block/branch/master/graph/badge.svg)](https://codecov.io/gl/nicolalandro/ai_block)
# AI Block
This repo contain the realization of this [Spike](https://gitlab.com/nicolalandro/spike-ai-gui). 
The project is a graphic tool to create artificial intelligence.

# Download and Use
WIP, a beta or alpha version does not exists yet.

# How to Setup Development
## Docker
Install [Docker CE](https://docs.docker.com/install/)  in order to have all needed requirements without compatibility problem. And if you are on linux install also [Docker Compose](https://docs.docker.com/compose/install/)

To run the code as production exec:

    $ docker-compose -f prod-docker-compose.yml up
To run the code as development exec: 
    
    $ docker-compose -f dev-docker-compose.yml up

## Pycharm
The recomended ide is [Pycharm Community](https://www.jetbrains.com/pycharm/download/).

If you use ubuntu like distro, you should to change something in order to use docker
- go to Settings >> Build, Execution, Deployment >> Docker
- select "TCP socket"
- copy 'unix:///var/run/docker.sock' under "Engine API URL"
## Without Docker
If you do not want to use docker, you need to install:
- [python](https://www.python.org/downloads/) version 3.5
- [pip](https://pip.pypa.io/en/stable/installing/)
- [nodejs](https://nodejs.org/en/download/) verison 8.12.0
- [npm](https://www.npmjs.com/get-npm)

To run the code exec

    $ pip install -r requirements.txt
    $ npm install --prefix vue_app/
    $ honcho start

For development add:
    $ pip install -r test_requirements.txt

# Tasks
The task are created using shovel:
* run test `shovel test`
* show coverage of last runned test `shovel cov`
* run test, show coverage and push to [codecov.io](https://codecov.io/gl/nicolalandro/ai_block) `shovel test_code_cov`
    
# Intresting Reading
- [UI Testing with Nightwatch.js, Headless Chrome, and Docker: Part 1](https://medium.com/@kenfehling/ui-testing-with-nightwatch-js-headless-chrome-and-docker-part-1-f0ce2e8a23a1)
- [How can I combine Vue.js with Flask?](https://stackoverflow.com/questions/46214132/how-can-i-combine-vue-js-with-flask)
