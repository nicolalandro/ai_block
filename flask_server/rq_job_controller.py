import requests

from flask_rq2 import RQ

rq = RQ()


@rq.job('exec_queue', timeout=-1)
def exec_graph_job(subscription_name, g):
    output = g.play()
    requests.post('http://localhost:5000/api/test', json={'data': output, 'name': subscription_name})
    return output
