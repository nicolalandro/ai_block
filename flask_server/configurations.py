class ProductionConfig(object):
    """
    Production specific config
    """
    DEBUG = False


class DevelopmentConfig(object):
    """
    Development environment specific configuration
    """
    TEMPLATES_AUTO_RELOAD = True
    DEBUG = True
    TESTING = True
