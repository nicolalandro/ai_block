class Graph:
    def __init__(self, graph):
        self.graph = graph

    def play(self):
        output_text = ''
        variable_dict = {}
        plan = self.execution_plan()
        for id, dependency in plan:
            pycode = self.get_pycode_by_id(id)
            input_values = self.create_input(id, dependency, variable_dict)
            output_values = globals()
            try:
                exec(pycode, input_values, output_values)
                variable_dict[id] = output_values
                if 'out' in output_values:
                    output_text += str(output_values['out']) + '\n'
            except Exception as e:
                return output_text + 'ERROR on node ' + str(id) + ':\n' + str(e)

        return output_text

    def execution_plan(self):
        edges = self.graph['elements']['edges']
        nodes = self.graph['elements']['nodes']

        to_process_edges = edges
        to_process_nodes = nodes
        processed_edges = []

        runnable_order = []
        processable_node_id = 0
        while len(to_process_nodes) != 0 or len(to_process_edges) != 0:
            processable_nodes_id = self.processable_nodes(to_process_edges, to_process_nodes)
            to_process_nodes = list(filter(lambda n: n['data']['id'] not in processable_nodes_id, to_process_nodes))
            for pn in processable_nodes_id:
                mapVariables = []
                for edge in processed_edges:
                    if edge['data']['target'] == pn:
                        mapVariables.append((edge['data']['source'], edge['data']['mapVariable']))
                runnable_order.append((pn, mapVariables))
            processable_id = runnable_order[processable_node_id][0]
            processable_node_id += 1
            processable_edges_id = self.processable_edges(processable_id, to_process_edges)
            processed_edges += list(filter(lambda e: e['data']['id'] in processable_edges_id, to_process_edges))
            to_process_edges = list(filter(lambda e: e['data']['id'] not in processable_edges_id, to_process_edges))

        return runnable_order

    def processable_nodes(self, edges, nodes):
        nodes_ids = set(map(lambda n: n['data']['id'], nodes))
        target_nodes = set(map(lambda e: e['data']['target'], edges))
        return list(nodes_ids - target_nodes)

    def processable_edges(self, processable_id, edges):
        processable_edges = list(filter(lambda e: e['data']['source'] == processable_id, edges))
        return list(map(lambda e: e['data']['id'], processable_edges))

    def get_pycode_by_id(self, id):
        pycode = ''
        for d in self.graph['elements']['nodes']:
            if d['data']['id'] == id:
                pycode = d['data']['pyCode']
        return pycode

    def create_input(self, id, dependency, variable_dict):
        input_val = globals()
        for dependency_id, map_vars in dependency:
            for map_var in list(filter(None, map_vars.split(';'))):
                value = map_var.split(':')
                input_val[value[1]] = variable_dict[dependency_id][value[0]]
        node = list(filter(lambda n: n['data']['id'] == id, self.graph['elements']['nodes']))[0]['data']
        if 'properties' in node:
            for k in node['properties']:
                input_val[k] = node['properties'][k]
        return input_val
