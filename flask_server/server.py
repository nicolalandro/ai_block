import json
import os

from flask import render_template, Flask, send_from_directory, request, jsonify

import flask_server
from flask_server.graph import Graph

from flask_server import rq_job_controller

from flask_socketio import SocketIO, emit


def abs_path(relative_path):
    base_path = os.path.dirname(os.path.dirname(os.path.realpath(flask_server.__file__)))
    return base_path + relative_path


def ls_walk_throw(project_folder):
    files = []
    for file_names in os.listdir(project_folder):
        file_path = os.path.join(project_folder, file_names)
        if os.path.isdir(file_path):
            files.append({file_names: ls_walk_throw(file_path)})
        else:
            files.append(file_names)
    return files


app = Flask(__name__, template_folder=abs_path('/static'))
rq_job_controller.rq.init_app(app)
socketio = SocketIO(app)

# API
PROJECTS_PATH = '/static/projects'


@socketio.on('my out', namespace='/api/execution_output')
def test_message(message):
    emit(message['name'], message['data'], broadcast=True)


@socketio.on('connect', namespace='/api/execution_output')
def test_connect():
    print('Client connected')


@socketio.on('disconnect', namespace='/api/execution_output')
def test_disconnect():
    print('Client disconnected')


@app.route('/api/test', methods=['POST'])
def test_message():
    req = request.get_json()
    emit(req['name'], req['data'], broadcast=True, namespace='/api/execution_output')
    return 'ok'


@app.route('/api/exec', methods=['POST'])
def exec_graph():
    req = request.get_json()
    subscription_name = req['subscription_name']
    graph = req['graph']
    g = Graph(graph)
    rq_job_controller.exec_graph_job.queue(subscription_name, g)
    return str('job started')


@app.route('/api/saved_projects')
def saved_projects():
    all_projects_folder = abs_path(PROJECTS_PATH)
    create_folder_if_not_exists(all_projects_folder)
    files = os.listdir(all_projects_folder)
    return jsonify(files)


@app.route('/api/project/<project_name>')
def project_content(project_name):
    try:
        all_projects_folder = abs_path(PROJECTS_PATH)
        project_folder = os.path.join(all_projects_folder, project_name)
        files = ls_walk_throw(project_folder)
        graphs = list(filter(lambda x: isinstance(x, str) and x.endswith('.json.graph'), files))
        graphs = list(map(lambda x: x[:-11], graphs))
        return jsonify({'files': files, 'graphs': graphs})
    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)})


@app.route('/api/new_project', methods=['POST'])
def new_project():
    all_projects_folder = abs_path(PROJECTS_PATH)
    create_folder_if_not_exists(all_projects_folder)

    req = request.get_json()
    project_name = req['name']
    project_folder = os.path.join(all_projects_folder, project_name)
    if os.path.isdir(project_folder):
        response = jsonify('Project "%s" already exists' % project_name)
        response.status_code = 400
        return response
    else:
        os.mkdir(project_folder)
        return jsonify('SUCCESS')


def create_folder_if_not_exists(all_projects_folder):
    if not os.path.isdir(all_projects_folder):
        os.mkdir(all_projects_folder)


@app.route('/api/save_graph', methods=['POST'])
def save_graph():
    req = request.get_json()
    project_name = req['project_name']
    graph_name = req['graph_name']
    graph = req['graph']
    all_projects_folder = abs_path(PROJECTS_PATH)
    path = os.path.join(all_projects_folder, project_name, graph_name + '.json.graph')
    with open(path, 'w') as outfile:
        json.dump(graph, outfile)
    return jsonify('SUCCESS')


@app.route('/api/load_graph', methods=['POST'])
def load_graph():
    req = request.get_json()
    try:
        project_name = req['project_name']
        graph_name = req['graph_name']
        all_projects_folder = abs_path(PROJECTS_PATH)
        path = os.path.join(all_projects_folder, project_name, graph_name + '.json.graph')
        with open(path, 'r') as infile:
            js = infile.read()
        return jsonify({'status': 'ok', 'graph': js, 'message': 'SUCCESS'})
    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)})


@app.route('/api/save_node', methods=['POST'])
def save_node():
    req = request.get_json()
    project_name = req['project_name']
    node_name = req['node_name']
    graph = req['graph']
    all_projects_folder = abs_path(PROJECTS_PATH)
    projects_node_folder = os.path.join(all_projects_folder, project_name, 'nodes')
    if not os.path.isdir(projects_node_folder):
        os.mkdir(projects_node_folder)
    path = os.path.join(projects_node_folder, node_name + '.json.node')
    with open(path, 'w') as outfile:
        json.dump(graph, outfile)
    return jsonify('SUCCESS')


@app.route('/api/saved_nodes')
def saved_nodes():
    result = []
    # built-in
    built_in_nodes = os.listdir(abs_path('/static/nodes'))
    built_in_nodes = list(map(lambda x: x[:-10], built_in_nodes))
    result.append({'project_name': 'built-in-nodes', 'nodes': built_in_nodes})
    # created
    all_projects_folder = abs_path(PROJECTS_PATH)
    create_folder_if_not_exists(all_projects_folder)
    if len(os.listdir(all_projects_folder)) != 0:
        files = os.listdir(all_projects_folder)
        for file in files:
            if 'nodes' in os.listdir(os.path.join(all_projects_folder, file)):
                nodes = os.listdir(os.path.join(all_projects_folder, file, 'nodes'))
                nodes = list(map(lambda x: x[:-10], nodes))
                result.append({'project_name': file, 'nodes': nodes})
    return jsonify(result)


@app.route('/api/load_node', methods=['POST'])
def load_node():
    req = request.get_json()
    try:
        project_name = req['project_name']
        node_name = req['node_name']
        if project_name == 'built-in-nodes':
            builtin_nodes_folder = abs_path('/static')
            path = os.path.join(builtin_nodes_folder, 'nodes', node_name + '.json.node')
        else:
            all_projects_folder = abs_path(PROJECTS_PATH)
            path = os.path.join(all_projects_folder, project_name, 'nodes', node_name + '.json.node')
        with open(path, 'r') as infile:
            js = infile.read()
        return jsonify({'status': 'ok', 'node': js, 'message': 'SUCCESS'})
    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)})


# Assets (js, css, img, favicon.ico)
@app.route('/<path:path>')
def favicon(path):
    return send_from_directory(abs_path('/static/'), path)


# APP
@app.route('/')
def index():
    return render_template('index.html')


@app.route('/ws_test')
def test():
    return render_template('ws_test.html')
