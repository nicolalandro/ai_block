import os
import shutil

from shovel import task

STATIC_FOLDER = 'static'

SPIKE_STATIC_FOLDER = 'spike_vue_app/static'
INDEX = 'spike_vue_app/templates/index.html'
WS_TEST = 'spike_vue_app/templates/ws_test.html'
FAVICO = 'spike_vue_app/static/img/favicon.png'


@task
def build():
    """
    This create static folder fro ai_block server. Esamples: shovel spike.build
    """
    if os.path.isdir(STATIC_FOLDER):
        shutil.rmtree(STATIC_FOLDER)
    shutil.copytree(SPIKE_STATIC_FOLDER, STATIC_FOLDER)
    shutil.copy(INDEX, 'static/index.html')
    shutil.copy(WS_TEST, 'static/ws_test.html')
    shutil.copy(FAVICO, 'static/favicon.png')
