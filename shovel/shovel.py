import os

from shovel import task


@task
def test():
    """
    This execute all test. Esamples: shovel test
    """
    test_comand()


@task
def cov():
    """
    This execute print the test coverage. Esamples: shovel cov
    """
    print_coverage_comand()


@task
def test_code_cov():
    """
    This execute all test and push to server the codecoverage. Esamples: shovel test_code_cov
    """
    test_comand()
    print_coverage_comand()
    os.system('codecov --commi=$COMMIT')


def test_comand():
    os.system('coverage run --source=flask_server -m mamba.cli')


def print_coverage_comand():
    os.system('coverage report -m')
