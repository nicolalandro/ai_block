import os

from shovel import task


@task
def registry_login():
    """
    This login to gitlab registry. Esamples: shovel docker.registry_login
    """
    os.system('docker login registry.gitlab.com')


@task
def run():
    """
    This task run docker container bulded. Esamples: shovel docker.run
    """
    os.system('docker run -p 5000:5000 -p 6006:6006 registry.gitlab.com/nicolalandro/ai_block:$(git rev-parse HEAD)')


@task
def build():
    """
    This build a dockerfile from current git head. Esamples: shovel docker.build
    """
    build_comand()


@task
def release():
    """
    This release the current git head container. Esamples: shovel docker.release
    """
    build_comand()
    release_comand()


def build_comand():
    os.system('shovel spike.build')
    os.system('docker build -t registry.gitlab.com/nicolalandro/ai_block:$(git rev-parse HEAD) -f docker/Dockerfile .')


def release_comand():
    os.system('docker push registry.gitlab.com/nicolalandro/ai_block:$(git rev-parse HEAD)')
