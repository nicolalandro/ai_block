import os
import shutil
from time import sleep

from expects import expect, equal
from mamba import description, before, it, after
from selenium import webdriver
import multiprocessing

from flask_server.server import app
import spec.fixtures.helper as fixture_helper

if __name__ == '__main__':
    os.system('mamba %s' % __file__)
    exit()


def _server_process(app):
    def run_app():
        app.run(port=3737)

    process = multiprocessing.Process(
        target=run_app
    )
    return process


with description('App') as self:
    with before.all:
        self.all_projects_folder = fixture_helper.abs_path('/static/projects')
        if os.path.isdir(self.all_projects_folder):
            shutil.rmtree(self.all_projects_folder)

        self.process = _server_process(app)
        self.process.start()

        chrome_opt = webdriver.ChromeOptions()
        chrome_opt.headless = False
        self.browser = webdriver.Chrome('/home/mint/chrome_driver/chromedriver', options=chrome_opt)
    with after.all:
        self.browser.quit()
        self.process.terminate()

    with it('load index'):
        self.browser.get("http://localhost:3737")
        self.browser.find_element_by_id('project-input-id').send_keys('new_project_name')
        self.browser.find_element_by_id('create-project-button').click()
        sleep(1)
        console = self.browser.find_element_by_id('console')
        text = console.find_elements_by_xpath(".//div[contains(@class, 'card-body')]")[0].text
        expect(text).to(equal('"SUCCESS"'))
