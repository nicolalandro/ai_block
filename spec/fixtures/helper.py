import json
import os

import flask_server


def abs_path(file_path):
    base_path = os.path.dirname(os.path.dirname(os.path.realpath(flask_server.__file__)))
    return base_path + file_path


def relative_path(file_path):
    base_path = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(base_path, file_path)


def get_dict_from_json(file_name):
    with open(relative_path(file_name), 'r') as file:
        return json.loads(file.read())


def get_string_from_file(file_name):
    with open(relative_path(file_name), 'r') as file:
        return file.read()


def get_string_from_absolute_path(file_name):
    with open(file_name, 'r') as file:
        return file.read()
