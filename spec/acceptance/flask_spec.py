import os
import shutil

from expects import expect, equal, contain
from mamba import description, before, it

import flask_server
import spec.fixtures.helper as fixture_helper
from flask_server.server import app, abs_path
import json

if __name__ == '__main__':
    os.system('mamba %s' % __file__)
    exit()


def back_dir(n, path):
    new_path = path
    for _ in range(n):
        new_path = os.path.dirname(new_path)
    return new_path


with description(app) as self:
    with before.all:
        self.tester = app.test_client(self)

        self.all_projects_folder = fixture_helper.abs_path('/static/projects')
        if os.path.isdir(self.all_projects_folder):
            shutil.rmtree(self.all_projects_folder)
        self.project_name = 'project_name'
        self.project_folder = os.path.join(self.all_projects_folder, self.project_name)

    with it('get correct path'):
        p = abs_path('/static')
        expected_path = os.path.dirname(os.path.dirname(os.path.realpath(flask_server.__file__))) + '/static'
        expect(p).to(equal(expected_path))

    with it('serve index'):
        response = self.tester.get('/', content_type='html/text')
        expect(response.status_code).to(equal(200))

    with it('exec a graph correctly'):
        json = '{"subscription_name":"name","graph":' + fixture_helper.get_string_from_file('flow_one_node.json') + '}'
        response = self.tester.post('/api/exec', content_type='application/json', data=json)
        expect(response.status_code).to(equal(200))
        expect(response.data).to(equal(b'job started'))

    with it('serve css file'):
        css_path = os.path.join(back_dir(3, __file__), 'static/css')
        css_file_name = os.listdir(css_path)[0]
        http_img_path = '/css/%s' % css_file_name
        css_file_content = fixture_helper.get_string_from_absolute_path(os.path.join(css_path, css_file_name))
        response = self.tester.get(http_img_path)
        expect(response.status_code).to(equal(200))
        expect(response.get_data(as_text=True)).to(equal(css_file_content))

    with it('serve js file'):
        js_path = os.path.join(back_dir(3, __file__), 'static/js')
        js_file_name = os.listdir(js_path)[0]
        http_img_path = '/js/%s' % js_file_name
        js_file_content = fixture_helper.get_string_from_absolute_path(os.path.join(js_path, js_file_name))
        response = self.tester.get(http_img_path)
        expect(response.status_code).to(equal(200))
        expect(response.get_data(as_text=True)).to(equal(js_file_content))

    with it('serve img file'):
        img_path = os.path.join(back_dir(3, __file__), 'static/img')
        img_file_name = list(filter(lambda name: '.' in name, os.listdir(img_path)))[0]
        http_img_path = '/img/%s' % img_file_name
        response = self.tester.get(http_img_path)
        expect(response.status_code).to(equal(200))

    with it('serve favicon.ico'):
        img_path = os.path.join(back_dir(3, __file__), 'static/')
        img_file_name = 'favicon.png'
        http_img_path = '/%s' % img_file_name
        response = self.tester.get(http_img_path)
        expect(response.status_code).to(equal(200))
    with description('api/new_project'):
        with it('create project folder'):
            json_string = '{"name": "%s"}' % self.project_name
            response = self.tester.post('/api/new_project', content_type='application/json', data=json_string)
            expect(response.status_code).to(equal(200))
            expect(response.get_data(as_text=True)).to(equal('"SUCCESS"\n'))

            response = self.tester.post('/api/new_project', content_type='application/json', data=json_string)
            expect(response.status_code).to(equal(400))
            expect(response.get_data(as_text=True)).to(equal('"Project \\"project_name\\" already exists"\n'))
    with description('/api/saved_projects'):
        with it('returns all projects name'):
            # create project
            json_string = '{"name": "%s"}' % self.project_name
            self.tester.post('/api/new_project', content_type='application/json', data=json_string)

            response = self.tester.get('/api/saved_projects', content_type='application/json', data=json_string)
            expect(response.status_code).to(equal(200))
            expect(response.get_data(as_text=True)).to(equal('["project_name"]\n'))
    with description('api/save_graph'):
        with it('create graph file'):
            # create project
            json_string = '{"name": "%s"}' % self.project_name
            self.tester.post('/api/new_project', content_type='application/json', data=json_string)

            json_string = '{"project_name": "%s", "graph_name": "graph_name", "graph":{}}' % self.project_name
            response = self.tester.post('/api/save_graph', content_type='application/json', data=json_string)
            expect(response.status_code).to(equal(200))
            expect(response.get_data(as_text=True)).to(equal('"SUCCESS"\n'))
            os.path.isfile(os.path.join(self.project_name, 'graph_name.json.graph'))
    with description('api/load_graph'):
        with it('load graph'):
            # create project
            json_string = '{"name": "%s"}' % self.project_name
            self.tester.post('/api/new_project', content_type='application/json', data=json_string)
            # create graph
            json_string = '{"project_name": "%s", "graph_name": "graph_name", "graph":{}}' % self.project_name
            self.tester.post('/api/save_graph', content_type='application/json', data=json_string)

            json_string = '{"project_name": "%s", "graph_name": "graph_name"}' % self.project_name
            response = self.tester.post('/api/load_graph', content_type='application/json', data=json_string)
            expect(response.status_code).to(equal(200))
            expect(response.get_data(as_text=True)).to(equal('{"graph":"{}","message":"SUCCESS","status":"ok"}\n'))
        with it('does not load graph if does not exists'):
            # shure to remove the existing projects
            self.all_projects_folder = fixture_helper.abs_path('/static/projects')
            if os.path.isdir(self.all_projects_folder):
                shutil.rmtree(self.all_projects_folder)
            json_string = '{"project_name": "%s", "graph_name": "graph_name"}' % self.project_name
            response = self.tester.post('/api/load_graph', content_type='application/json', data=json_string)
            expect(response.status_code).to(equal(200))
            expect(response.get_data(as_text=True)).to(contain('No such file or directory'))
    with description('api/save_node'):
        with it('create node file'):
            # create project
            json_string = '{"name": "%s"}' % self.project_name
            self.tester.post('/api/new_project', content_type='application/json', data=json_string)

            json_string = '{"project_name": "%s", "node_name": "node_name", "graph":{}}' % self.project_name
            response = self.tester.post('/api/save_node', content_type='application/json', data=json_string)
            expect(response.status_code).to(equal(200))
            expect(response.get_data(as_text=True)).to(equal('"SUCCESS"\n'))
            os.path.isfile(os.path.join(self.project_name, 'nodes/node_name.json.node'))
    with description('/api/saved_nodes'):
        with it('create node file'):
            # create project
            json_string = '{"name": "%s"}' % self.project_name
            self.tester.post('/api/new_project', content_type='application/json', data=json_string)
            # create node
            json_string = '{"project_name": "%s", "node_name": "node_name", "graph":{}}' % self.project_name
            self.tester.post('/api/save_node', content_type='application/json', data=json_string)

            response = self.tester.get('/api/saved_nodes', content_type='application/json', data=json_string)
            expect(response.status_code).to(equal(200))
            response_body = json.loads(response.get_data(as_text=True))
            expect(len(response_body)).to(equal(2))
            expect(response_body[1]).to(
                equal({"nodes": ["node_name"], "project_name": "project_name"}))
    with description('/api/load_node'):
        with it('get the requested node'):
            # create project
            json_string = '{"name": "%s"}' % self.project_name
            self.tester.post('/api/new_project', content_type='application/json', data=json_string)
            # create node
            json_string = '{"project_name": "%s", "node_name": "node_name", "graph":{}}' % self.project_name
            self.tester.post('/api/save_node', content_type='application/json', data=json_string)

            json_string = '{"project_name": "%s", "node_name": "node_name"}' % self.project_name
            response = self.tester.post('/api/load_node', content_type='application/json', data=json_string)
            expect(response.status_code).to(equal(200))
            expect(response.get_data(as_text=True)).to(equal('{"message":"SUCCESS","node":"{}","status":"ok"}\n'))
        with it('does not load node if does not exists'):
            # shure to remove the existing projects
            self.all_projects_folder = fixture_helper.abs_path('/static/projects')
            if os.path.isdir(self.all_projects_folder):
                shutil.rmtree(self.all_projects_folder)
            json_string = '{"project_name": "%s", "node_name": "node_name"}' % self.project_name
            response = self.tester.post('/api/load_node', content_type='application/json', data=json_string)
            expect(response.status_code).to(equal(200))
            expect(response.get_data(as_text=True)).to(contain('No such file or directory'))
    with description('api/project/<project_name>'):
        with it('get all files'):
            json_string = '{"name": "%s"}' % self.project_name
            self.tester.post('/api/new_project', content_type='application/json', data=json_string)
            json_string = '{"project_name": "%s", "node_name": "node_name", "graph":{}}' % self.project_name
            self.tester.post('/api/save_node', content_type='application/json', data=json_string)
            json_string = '{"project_name": "%s", "graph_name": "graph_name", "graph":{}}' % self.project_name
            self.tester.post('/api/save_graph', content_type='application/json', data=json_string)

            response = self.tester.get('/api/project/%s' % self.project_name)
            expect(response.status_code).to(equal(200))
            expect(json.loads(response.get_data(as_text=True))).to(
                equal(
                    {'graphs': ['graph_name'],
                     'files': ["graph_name.json.graph", {"nodes": ["node_name.json.node"]}]}))
