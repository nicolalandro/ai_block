import os

from expects import expect, equal
from mamba import description, it

import spec.fixtures.helper as fixture_helper
from flask_server.graph import Graph

if __name__ == '__main__':
    os.system('mamba %s' % __file__)
    exit()

with description(Graph):
    with description('.execution_plan()'):
        with it('is able to plan one node'):
            json = fixture_helper.get_dict_from_json('flow_one_node.json')
            g = Graph(json)
            plan = g.execution_plan()
            expect(plan).to(equal([('1', [])]))

        with it('is able to plan two indipendent node'):
            json = fixture_helper.get_dict_from_json('flow_two_indipendent_nodes.json')
            g = Graph(json)
            plan = g.execution_plan()
            if plan[0][0] == '2':
                expect(plan).to(equal([('2', []), ('1', [])]))
            else:
                expect(plan).to(equal([('1', []), ('2', [])]))
        with it('is able to plan two dipendent node'):
            json = fixture_helper.get_dict_from_json('flow_two_nodes.json')
            g = Graph(json)
            plan = g.execution_plan()
            expect(plan).to(equal([('2', []), ('1', [('2', 'test2var:test1var;')])]))
        with it('is able to plan three nodes'):
            json = fixture_helper.get_dict_from_json('flow_three_nodes.json')
            g = Graph(json)
            plan = g.execution_plan()
            if plan[0][0] == '2':
                expect(plan).to(
                    equal(
                        [('2', []), ('3', []), ('1', [('2', 'test2var:test1var1;'), ('3', 'test3var:test1var2;')])]
                    )
                )
            else:
                expect(plan).to(
                    equal(
                        [('3', []), ('2', []), ('1', [('3', 'test3var:test1var2;'), ('2', 'test2var:test1var1;')])]
                    )
                )
    with description('.play()'):
        with it('run one node'):
            json = fixture_helper.get_dict_from_json('flow_one_node.json')
            g = Graph(json)
            output = g.play()
            expect(output).to(equal('node1 end\n'))
        with it('run two indipendent node'):
            json = fixture_helper.get_dict_from_json('flow_two_indipendent_nodes.json')
            g = Graph(json)
            output = g.play()
            if output.startswith('node1'):
                expect(output).to(equal('node1 out\nnode2 out\n'))
            else:
                expect(output).to(equal('node2 out\nnode1 out\n'))
        with it('run two dipendent node'):
            json = fixture_helper.get_dict_from_json('flow_two_nodes.json')
            g = Graph(json)
            output = g.play()
            expect(output).to(equal('node2 out\nnode1 receive: node2 text\n'))
        with it('run three node'):
            json = fixture_helper.get_dict_from_json('flow_three_nodes.json')
            g = Graph(json)
            output = g.play()
            expect(output).to(equal("node1 receive: from node 2: node3 text, from node 3: node2 text\n"))
        with it('return error when thrown'):
            json = fixture_helper.get_dict_from_json('flow_one_node_with_pycode_error.json')
            g = Graph(json)
            output = g.play()
            expect(output).to(equal('ERROR on node 1:\ninvalid syntax (<string>, line 1)'))

    with description('.get_pycode_by_id'):
        with it('return the pycode of the passed id'):
            json = fixture_helper.get_dict_from_json('flow_two_indipendent_nodes.json')
            g = Graph(json)
            expect(g.get_pycode_by_id('1')).to(equal("# a comment\nout='node1 out'"))
            expect(g.get_pycode_by_id('2')).to(equal("# a comment\nout='node2 out'"))

    with description('.create_input'):
        with it('return input dict'):
            json = fixture_helper.get_dict_from_json('flow_three_nodes.json')
            g = Graph(json)

            variable_dict = {
                '2': {
                    'test2var': 'test1var2'
                },
                '3': {
                    'test3var': 'test1var1'
                }
            }
            dependencies = [('3', 'test3var:test1var2;'), ('2', 'test2var:test1var1;')]
            input_val = g.create_input('1', dependencies, variable_dict)
            expect(input_val).to(equal({'test1var2': 'test1var1', 'test1var1': 'test1var2'}))
        with it('return input dict with multiple variable'):
            json = fixture_helper.get_dict_from_json('flow_two_nodes_with_multiple_connection.json')
            g = Graph(json)

            variable_dict = {
                '2': {
                    'test2var1': 'test1var1',
                    'tes2var2': 'test1var2'
                }
            }
            dependencies = [('2', 'test2var1:test1var1;tes2var2:test1var2')]
            input_val = g.create_input('1', dependencies, variable_dict)
            expect(input_val).to(equal({'test1var2': 'test1var2', 'test1var1': 'test1var1', 'prop': 'test'}))

    with description('.processable_nodes'):
        with it('process the no dependency nodes'):
            edges = [{'data': {'target': '1', 'id': '1', 'mapVariable': 'test2var:test1var;', 'source': '2'}}]
            nodes = [
                {'data':
                    {
                        'properties': {},
                        'id': '1',
                        'pyCode': "# not relevant",
                        'name': 'Test 1'
                    }
                },
                {'data':
                    {
                        'properties': {},
                        'id': '2',
                        'pyCode': "# not relevant",
                        'name': 'Test 2'
                    }
                }
            ]
            json = fixture_helper.get_dict_from_json('flow_two_nodes.json')
            g = Graph(json)
            expect(g.processable_nodes(edges, nodes)).to(equal(['2']))
    with description('.processable_edges'):
        with it('process the no dependency edges'):
            edges = [{'data': {'target': '1', 'id': '1', 'mapVariable': 'test2var:test1var;', 'source': '2'}}]
            json = fixture_helper.get_dict_from_json('flow_two_nodes.json')
            g = Graph(json)
            expect(g.processable_edges('2', edges)).to(equal(['1']))
