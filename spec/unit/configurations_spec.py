import os

from expects import expect, equal
from mamba import describe, it, before

from flask_server.configurations import DevelopmentConfig, ProductionConfig

if __name__ == '__main__':
    os.system('mamba %s' % __file__)
    exit()

with describe(DevelopmentConfig) as self:
    with before.all:
        self.config = DevelopmentConfig()
    with it('have debug enabled'):
        expect(self.config.DEBUG).to(equal(True))
    with it('have testing enabled'):
        expect(self.config.TESTING).to(equal(True))
    with it('have templates auto reload enabled'):
        expect(self.config.TEMPLATES_AUTO_RELOAD).to(equal(True))

with describe(ProductionConfig):
    with before.all:
        self.config = ProductionConfig()
    with it('have debug disabled'):
        expect(self.config.DEBUG).to(equal(False))
